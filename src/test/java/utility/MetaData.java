package utility;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Inherited
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({ METHOD, TYPE })
public @interface MetaData {
	/**
	 * The value for meta-data must be valid json map. For example:
	 * <P>
	 * <CODE>
	 * "{'Brand':['A'],'Module':['M1','M2']}"</CODE>
	 * 
	 * @return value of the MetaData
	 */
	String value() default "";
}

