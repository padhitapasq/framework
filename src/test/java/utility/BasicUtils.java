package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class BasicUtils {

	public static Object[][] extractData(String fileName, String dataSetArray) throws FileNotFoundException {

		List<TestData> testData;
		JsonElement dataSet;
		Object[][] returnValue;
		JsonElement jsonData = new JsonParser().parse(new FileReader(fileName));
		try {
			dataSet = jsonData.getAsJsonObject().get(dataSetArray);
			testData = new Gson().fromJson(dataSet, new TypeToken<List<TestData>>() {
			}.getType());
			returnValue = new Object[testData.size()][1];
		} catch (Exception e) {
			dataSet = jsonData.getAsJsonObject().get("defaultData");
			testData = new Gson().fromJson(dataSet, new TypeToken<List<TestData>>() {
			}.getType());
			returnValue = new Object[testData.size()][1];
		}
		
		int index = 0;
		for (Object[] each : returnValue) {
			each[0] = testData.get(index++);
		}
		return returnValue;
	}
	
	public static String getConfigProperty(String propertyName) {

		Properties prop = new Properties();
		FileInputStream configFIS = null;
		File configProp = new File(
				System.getProperty("user.dir") + "/src/test/java/com/project/configuration/config.properties");
		try {
			configFIS = new FileInputStream(configProp);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("config file not found");
		}
		try {
			prop.load(configFIS);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

		return prop.getProperty(propertyName);

	}
	
	public static String getObjectRepovalues(String propertyName,String pageName) {

		Properties prop = new Properties();
		FileInputStream configFIS = null;
		File configProp = new File(
				System.getProperty("user.dir") + "/src/test/java/com/project/objectrepo/"+pageName+".properties");
		try {
			configFIS = new FileInputStream(configProp);
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("config file not found");
		}
		try {
			prop.load(configFIS);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

		return prop.getProperty(propertyName);

	}
	
	public static void readExcel(String excelSheet) {

		try {
			Workbook workbook = WorkbookFactory.create(new File(excelSheet));

			System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

			Sheet sheet = workbook.getSheet("Sheet1");
			

			DataFormatter dataFormatter = new DataFormatter();

			System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
			
			int rowCount = sheet.getLastRowNum();
			System.out.println(rowCount);
			String lastcellData = null;
			
//			DataFormatter formatter = new DataFormatter();
//			for (Iterator iterator = sheet.rowIterator(); iterator.hasNext();) {
//			    XSSFRow row = (XSSFRow) iterator.next();
//			    for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
//			        XSSFCell cell = row.getCell(i);
//			        System.out.println(formatter .formatCellValue(cell));
//			        
//			    }
//
//			}
			
			Row row = null;
			Cell cell = null;
			for( int i = 0; i < rowCount+1 ; i++) {
				System.out.println("inRow Number" + i);
				row = sheet.getRow(i);
				int cellCount = row.getLastCellNum()-row.getFirstCellNum();
				System.out.println(cellCount);

				for(int j=0; j<cellCount; j++) {
					cell = row.getCell(j);
					String cellValue = dataFormatter.formatCellValue(cell);
					System.out.println(cellValue);
					if(j==cellCount-1) {
						lastcellData = cellValue;
					}
					
				}
				System.out.println("Last Cell value is " + lastcellData);
			}
			
			
//			Iterator<Row> rowIterator = sheet.rowIterator();
//			while (rowIterator.hasNext()) {
//				Row row = rowIterator.next();
//
//				// Now let's iterate over the columns of the current row
//				Iterator<Cell> cellIterator = row.cellIterator();
//
//				while (cellIterator.hasNext()) {
//					Cell cell = cellIterator.next();
//					String cellValue = dataFormatter.formatCellValue(cell);
//					System.out.print(cellValue + "\t");
//				}
//				System.out.println();
//			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
