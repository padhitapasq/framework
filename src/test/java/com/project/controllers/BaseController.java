package com.project.controllers;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.BasicUtils;

public class BaseController {

	BasicUtils baseUtils = new BasicUtils();
	public static WebDriver driver;
	Properties prop = null;

	public WebDriver getDriver() {
		if (driver == null) {
			try {
				switch (BasicUtils.getConfigProperty("browser").toUpperCase()) {

				case "CHROME":
					System.setProperty("webdriver.chrome.driver", "/Users/tapaspadhi/Documents/Eclipse/chromedriver");
					driver = new ChromeDriver();
					break;
				case "SAFARI":
					driver = new SafariDriver();
					break;
				default:
					System.out.println("No Driver has been selected");
					break;
				}
			} catch (Exception ex) {
				ex.getMessage();
			}
			navigateToUrl();
		}
		return driver;

	}

	public WebElement findElement(String elementName) {

		getDriver();
		WebElement element = null;
		String[] object;

		try {
			String pageName = super.getClass().getSimpleName().toLowerCase();

			String node = BasicUtils.getObjectRepovalues(elementName, pageName);
			object = node.split("\\|");

			switch (object[0]) {
			case "xpath":
				element = driver.findElement(By.xpath(object[1]));
				break;
			case "id":
				element = driver.findElement(By.id(object[1]));
				break;
			default:
				break;

			}
		} catch (Exception ex) {
			ex.getMessage();
		}

		return element;
	}

	public void clickOnElement(String elementName) {
		try {
			findElement(elementName).click();
		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	public void sendKeys(String elementName, String value) {
		try {
			findElement(elementName).sendKeys(value);
		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	public void navigateToUrl() {
		try {
			String url = BasicUtils.getConfigProperty("url");
			driver.get(url);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// not to be used, not tested yet
	public void waitForElement(String elementName) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(findElement(elementName)));
	}

	public void tearDown() {
		driver.quit();
	}
}
