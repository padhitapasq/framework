package com.project.testcases;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import utility.TestData;


public class Testngdemo extends BaseTest{
	
	@Test(dataProvider="getData")
	public void entry(TestData data) {
		log("Test case entry execution has been started");
		System.out.println(data.getUsername());
		System.out.println(data.getPassword());
		System.out.println(System.getProperty("user.dir") + "/executables/chromedriver");
	}
	
	@Test(dataProvider="getData")
	public void entry1(TestData data) {
		log("Test case entry1 execution has been started");

		System.out.println(data.getTestCase());
	}
	
	@Test(dataProvider="getData")
	public void entry2(TestData data) {
		log("Test case entry2 execution has been started");

		Assert.assertFalse(true);
		System.out.println(data.getTestCase());
	}
	
	@Test(dataProvider="getData")
	public void entry3(TestData data) {
		log("Test case entry3 execution has been started");

		throw new SkipException("Skipped");
	}
	 

}
