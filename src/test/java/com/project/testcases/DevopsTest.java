package com.project.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.project.controllers.DevOpsExampleController;

import utility.TestData;

public class DevopsTest extends BaseTest{
	
	DevOpsExampleController devopscontroller = new DevOpsExampleController();
	
	@Test(dataProvider="getData")
	public void verifyHeaderText(TestData data) {
		log("Test case verifyHeaderText execution has been started");
		Assert.assertTrue(devopscontroller.getHeaderText().equals(data.getHeadertext()));
	}

}
