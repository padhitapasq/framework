package com.project.testcases;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.project.controllers.BaseController;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utility.BasicUtils;

public class BaseTest {
	ExtentReports extent;
	ExtentTest test;
	BaseController basecontroller;
	public static String fileName = null;
	public static Object[][] returnValue;
	
	public BaseTest() {
		basecontroller = new BaseController();
	}

	@BeforeSuite
	public void beforeSuite() {
		extent = new ExtentReports(System.getProperty("user.dir") + "//test-output//ExtentReport.html", true);
		extent.addSystemInfo("Host Name", "Framework Development Class")
				.addSystemInfo("Environment", "Automation Testing").addSystemInfo("User Name", "Tapas Padhi");
		extent.loadConfig(new File(System.getProperty("user.dir") + "//extent-report.xml"));

	}

	@BeforeMethod
	public void beforeMethod(Method testMethod) {
		test = extent.startTest(testMethod.getName(), "Execution status of " + testMethod.getName());

	}

	@BeforeClass
	public void beforeClass() {
		String className = super.getClass().getSimpleName();
		fileName = "src/test/java/resources/" + className + ".json";
	}
	
	@AfterMethod
	public void afterMethod(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, "Test Case Failed is " + result.getName());
			test.log(LogStatus.FAIL, "Test Case Failed is " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test Case Skipped is " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(LogStatus.PASS, "Test Case Passed is " + result.getName());
		}
		extent.endTest(test);
	}

	@AfterSuite
	public void afterSuite() {
		extent.flush();
		extent.close();
		basecontroller.tearDown();
	}

	@DataProvider
	public Object[][] getData(Method testMethod) throws FileNotFoundException {
		return BasicUtils.extractData(fileName, testMethod.getName());
	}
	
	public void log(String logName) {
		test.log(LogStatus.INFO, logName);
	}

}
