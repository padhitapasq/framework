package com.project.testcases;

import org.testng.annotations.Test;

import com.project.controllers.ForgetAccountController;
import com.project.controllers.LoginController;

import utility.TestData;

public class Testngdemo2 extends BaseTest{

	@Test(dataProvider="getData")
	public void invokeBrowser(TestData data) {
		
//		System.out.println(System.getProperty("user.dir"));
		
		LoginController loginController = new LoginController();
		ForgetAccountController forgetAccountController = new ForgetAccountController();
		log("Test Case Strated");
		loginController.login();
		forgetAccountController.enterEmail("padhitapas.123@gmail.com");
		System.out.println(data.getTestCase());

	}
	
}
